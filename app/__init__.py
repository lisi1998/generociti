from flask import Flask
from flask_restful import Api
from flask_bcrypt import Bcrypt

from app.server.services.UserService import UserService
from app.server.config import config_by_name

api = Api()
app = Flask(__name__, template_folder='public/templates/', static_folder='public/static/')
flask_bcrypt = Bcrypt()
flask_bcrypt.init_app(app)
app.config.from_object(config_by_name['dev'])

api.add_resource(UserService, '/user/<string:id>')

from app.server import routes